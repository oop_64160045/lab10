package com.chanatda.week10;

public class Rectangle extends Shape {
    private double width;
    private double hight;

    public Rectangle(double width, double hight) {
        super("Rectangle");
        this.width = width;
        this.hight = hight;

    }

    public double getWidth() {
        return width;

    }

    public double getHeight() {
        return hight;
    }

    @Override
    public String toString() {
        return this.getName() + " width : " + this.width + " higth : " + this.hight;
    }
    @Override
    public double calArea(){
        return this.width*this.hight;
    }
    @Override
    public double calPerimeter(){
        return this.width*2+this.hight*2;
    }


}
